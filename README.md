IMPORTANT: Following need to be updated in playbook / artifactory
1. Protector binary, analytics binary, pexpect binary, freetds binary,snmp files need to be uploaded in artifactory
2. Relevant Repo URL, user name, password, and checksum for respective files need to be updated in the roles
3. Management server IP, username and password, protector server ip need to be updated in config.yml file
4. If disk mount role is executed, update the disk name and mount point in config.yml
5. Update protector and analytics server name/IP in inventory file

Command to run playbook:

ansible-playbook -i inventory DLP.yml

Roles and tasks:

Below are the list of roles used in this playbook and activites performed by each role.

Selinux Role: selinux-disable

This role will disable selinux and reboot the machine.


DLP Protector:

Protector Prerequisites Role: redhat-protector-Prerequisites

Following task will be performed by the playbook

1. Stop Firewall and disable from boot
2. Ansible pexpect module installation (its required for automate interactive mode of installation in remote machine)
3. Check the windows machine is reachable to start the installation
4. Check two NIC  cards available to processed the installation
5. check /opt partition have minimum 45 GB of available space
6. set hostname and fully qualified name are identical 


Disk Mount Role: redhat-disk-mount ( Optional) 

Mount new disk in remote machine enable this role and modified the "disk name" and "Ansible mounts" 
variable value in config.yml

1. This role will created partition in remote machine
2. Change the file system 
3. Format the disk
4. Mount the disk
5. Make entry in fstab about the mount to mount the disk from boot

DLP Protector installation: protector-installation

1. Check protector is already installed or not
2. Download the protector installation file from artifactory and execute the same
3. install and configure snmp and chrony service


DLP Analytics Engine:


Analytics Prerequisites Role: redhat-analytics-Prerequisites

Following task will be performed by the playbook

1. Stop Firewall and disable from boot
2. set hostname and fully qualified name are identical 
3. Install freetds service
4. Ansible pexpect module installation (its required for automate interactive mode of installation in remote machine)
5. Installing the following packages apr, apr-util, perl-Switch, libtool-ltdl, unixODBC

DLP Analytics installation: analytics-installation

1. Check protector is already installed or not
2. Download the Analytics installation file from artifactory and execute the same
3. install and configure snmp and chrony service
